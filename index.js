/*

 Node.js library for Yate (Yet Another Telephone Engine) extmodule connection

 http://docs.yate.ro/wiki/External_module_command_flow

 Copyright (C) 2016 Vladimir Latyshev

 License
 DO WHAT YOU WANT TO

 */

var net = require('net'),
    events = require('events'),
    readline = require('readline'),
    path = require('path');


/*

 YateConnection class

 */

var YateConnection = function(socketConfig) {
    this.socketConfig = socketConfig;
};

YateConnection.prototype = new events.EventEmitter;


/*
 Methods
 */

YateConnection.prototype.connect = function(){
    var that = this;
    if (this.socketConfig) {
        // todo validate config
        if (this.connectSemaphore)
            return;
        this.connectSemaphore = true;
        if (this.reconnectInterval)
            clearInterval(this.reconnectInterval);
        this.reconnectSemaphore = false;

        this.socket = new net.Socket();

        this.socket.on("error", function (error) {
            that.connectSemaphore = false;
            this.destroy();
            that.emit("connection_error",error);
            if (that.socketConfig.reconnect)
                that.initReconnect();
        });

        this.socket.on('connect', function () {
            that.connectSemaphore = false;
            that.inStream = that.outStream = this;
            that.arrangeConnection();
        });

        this.socket.on('close', function () {
            that.connectSemaphore = false;
        });

        this.socket.on('end', function () {
            that.connectSemaphore = false;
            that.emit("connection_end");
            if (that.socketConfig.reconnect)
                that.initReconnect();
        });

        /*
         var timeout = timeout || 1000;
         this.socket.setTimeout(1000);
         this.socket.on('timeout', function () {
         that.emit("timeout");
         this.destroy();
         if (that.socketConfig.reconnect)
         that.initReconnect();
         });
         */

        this.socket.connect(this.socketConfig.port, this.socketConfig.host);
    }
    else {
        // stdin
        this.inStream = process.stdin;
        this.outStream =  process.stdout;
        setTimeout(YateConnection.prototype.arrangeConnection.bind(this), 200);
    }
};


YateConnection.prototype.initReconnect = function(line) {
    if (this.reconnectSemaphore)
        return;
    this.isConnected = false;
    this.isReady = false;
    this.emit("reconnect");
    this.reconnectSemaphore = true;
    var timeout = this.socketConfig.timeout || 500;
    var that = this;
    this.reconnectInterval = setInterval(function () {
        that.connect();
    }, timeout);
};


YateConnection.prototype.arrangeConnection = function() {
    var that = this;
    this.readlineInterface = readline.createInterface(this.inStream, this.outStream); //recreate readline each time on socket connect
    //todo destroy readline on disconnect
    this.readlineInterface.on('line', function (line) {
        that.processLine(line);
    });
    this.readlineInterface.on('close',function(){
        if (!that.socketConfig)
            that.emit("shutdown"); //only valid for stdin
    });
    this.isConnected = true;
    this.emit("connected");
};


YateConnection.prototype.processLine = function(line){

    this.emit('raw_message',line);

    var data_array = line.split(":");
    var message = new YateMessage();
    message._connection = this;

    switch (data_array[0]) {
        case "%%>message":
            //%%>message:<id>:<time>:<name>:<_retvalue>[:<key>=<value>...]
            message._type = 'incoming';
            message._id = data_array[1];
            message._origin = data_array[2];
            message._name = data_array[3];
            message._retval = Yate.unescape(data_array[4]);
            break;
        case "%%<message":
            //%%<message:<id>:<processed>:[<name>]:<_retvalue>[:<key>=<value>...]
            message._type = 'answer';
            message._id = data_array[1];
            message._handled = Yate.Str2bool(data_array[2]);
            message._name = data_array[3];
            message._retval = Yate.unescape(data_array[4]);
            break;
        case "%%<install":
            //%%<install:<priority>:<name>:<success>
            message._type = 'installed';
            message.priority = data_array[1];
            message._name = data_array[2];
            message.success = data_array[3];
            break;
        case "%%<uninstall":
            //%%<uninstall:<priority>:<name>:<success>
            message._type = 'uninstalled';
            message.priority = data_array[1];
            message._name = data_array[2];
            message.success = data_array[3];
            break;
        case "%%<watch":
            //%%<watch:<name>:<success>
            message._type = 'watched';
            message._name = data_array[1];
            message.success = data_array[2];
            break;
        case "%%<setlocal":
            //%%<setlocal:<name>:<value>:<success>
            message._type = 'setlocal';
            message._name = data_array[1];
            message.value = data_array[2];
            message.success = data_array[3];
            break;
        case "Error in":
            //Error in:
            this.emit("command_error",line);
            return;
        default:
            this.emit("command_unknown",line);
            return;
    }

    if (message._type == "incoming" || message._type == "answer" ) {
        data_array.slice(5).forEach(function (item) {
            var pos = item.indexOf("=");
            if (pos > 0) {
                var key = item.substr(0,pos);
                if (!(key in YateMessage.prototype)) {
                    var value = item.substr(pos + 1);
                    message.params[Yate.unescape(key)] = Yate.unescape(value);
                }
            }
        })
    }

    if (message._type == "answer" && message._name == 'database') {
        // Parse database message. Need patch for extmodule.yate
        // https://github.com/vir/yate/blob/patches/patches/extmodule_dumparray_variable
        var userData = [];
        var userDataArray = [];
        var data = Yate.unescape(message._retval).split('\n');
        data.pop(); // remove empty last row
        var keys = data.shift().split('\t');
        data.forEach(function(item,i){
            userData[i] = {};
            userDataArray[i] = [];
            var columns = item.split('\t');
            columns.forEach(function(item_,i_){
                userData[i][keys[i_]] = item_;
                userDataArray[i][i_] = item_;
            })
        });
        message.userData = userData;
        message.userDataArray = userDataArray;
        message._retval = '';
    }
    this.emit(message._type, message);
};


YateConnection.prototype.install = function(name, priority, filter, filterval) {
    //%%>install:[<priority>]:<name>[:<filter-name>[:<filter-value>]]
    if (filter && filterval)
        this.send('%%>install:' + priority + ':' + Yate.escape(name) + ':' + filter + ':' + filterval);
    else
        this.send('%%>install:' + priority + ':' + Yate.escape(name));
};


YateConnection.prototype.uninstall = function(name) {
    //%%>uninstall:<name>
    this.send('%%>uninstall:' + name);
};


YateConnection.prototype.Watch = function(name) {
    //%%>watch:<name>
    this.send('%%>watch:' + name);
};


YateConnection.prototype.Unwatch = function(name) {
    //%%>unwatch:<name>
    this.send('%%>unwatch:' + name)
};


YateConnection.prototype.output = function(string) {
    //%%>output:arbitrary unescaped string
    var that = this;
    string.split("\n").forEach(function (string) {
        that.send('%%>output:' + string);
    })
};


YateConnection.prototype.setlocal = function(name,value) {
    //%%>setlocal:<name>:<value>
    this.send('%%>setlocal:' + name + ':' + value);
};


YateConnection.prototype.send = function(string) {
    this.emit('raw_message',string);
    this.outStream.write(string + '\n');
};


YateConnection.prototype.createMessage = function(name) {
    var message = new YateMessage(name);
    message._connection = this;
    return message;
};


/*

 Yate static class, same as in libyate.php

 */

function Yate() {
    // static
}


Yate.escape = function(str, extra) {
    if (str === undefined)
        return "undefined";
    if (str === true)
        return "true";
    if (str === false)
        return "false";
    str = str.toString();
    var res = "";
    for (var idx=0; idx < str.length; idx++) {
        var chr = str.charAt(idx);
        if ((chr.charCodeAt(0) < 32) || (chr == ':') || (chr == extra)) {
            chr = String.fromCharCode(chr.charCodeAt(0) + 64);
            res += '%';
        }
        else if (chr == '%')
            res += chr;
        res += chr;
    }
    return res;
};


Yate.unescape = function(str)
{
    var res = "";
    for (var idx=0; idx < str.length; idx++) {
        var chr = str.charAt(idx);
        if (chr == '%') {
            idx++;
            chr = str.charAt(idx);
            if (chr != '%')
                chr = String.fromCharCode(chr.charCodeAt(0) - 64);
        }
        res += chr;
    }
    return res;
};


Yate.Bool2str = function(bool)
{
    return bool ? "true" : "false";
};


Yate.Str2bool = function(str)
{
    return (str == "true");
}


/*

 YateMessage class

 */

function YateMessage(name) {
    this._name = name;
    this._origin = Math.floor(Date.now() / 1000).toString();
    this._id = this._origin + process.hrtime()[1];
    this._type = "outgoing";
    this._handled = false;
    this.params = {};
}

var internalMessageParameters = {
    //params: {},
    _type: undefined,
    _id: undefined,
    _origin: undefined,
    _name: undefined,
    handlers: undefined,
    _handled: undefined,
    _retval: undefined,
    _connection: undefined,
};

YateMessage.prototype = internalMessageParameters;


/*
 Methods
 */

YateMessage.prototype.dispatch = function() {
    // %%>message:<id>:<time>:<name>:<_retvalue>[:<key>=<value>...]
    if (this._type != "outgoing")
        return;
    var rawMessage = '%%>message:' + Yate.escape(this._id) + ':' + this._origin + ':' + Yate.escape(this._name) + ':' + this.fillparams();
    this._connection.send(rawMessage);
    delete this._connection;
    this._type = "enqueued";
};


YateMessage.prototype.acknowledge = function() {
    //%%<message:<id>:<processed>:[<name>]:<_retvalue>[:<key>=<value>...]
    if (this._type != "incoming")
        return; // todo callback?
    var rawMessage = "%%<message:" + Yate.escape(this._id) + ":" + Yate.Bool2str(this._handled) + "::" + Yate.escape(this._retval) + this.fillparams(true);
    this._connection.send(rawMessage);
    delete this._connection;
    //Engine.logger.debug(this);
    this._type = "acknowledged";
};


YateMessage.prototype.fillparams = function(includeEmpty) {
    var result = '';
    for (var key in this.params) {
        if (this.params[key])
            result += ':' + Yate.escape(key) + '=' + Yate.escape(this.params[key]);
        else if (includeEmpty)
            result += ':' + Yate.escape(key);
    }
    return result;
};

/*
YateMessage.prototype.params = function() {
    var printable = {};
    for (var key in this) {
        if (!(key in this.__proto__)) {
            printable[key]=this[key];
        }
    }
    return printable;
};*/


/*

Node.js library for Yate (Yet Another Telephone Engine) Javascript language imitation

http://docs.yate.ro/wiki/Javascript

Copyright (C) 2016 Vladimir Latyshev

       License
 DO WHAT YOU WANT TO

*/

var moduleName = "nodejs";

var crypto = require('crypto');
var fs = require('fs');
var PATH = require('path');
var vm = require('vm');
var clc = require('cli-color');
var log4js = require('log4js');
var DOMParser = require('xmldom').DOMParser;
var XMLSerializer = require('xmldom').XMLSerializer;



/*

 Engine class

 http://docs.yate.ro/wiki/Javascript_Engine

*/

function Engine() {
    // static object, no constructor

}

const DEBUG_LEVEL_NAMES = [undefined,"TEST","GOON","CONF","STUB","WARN","MILD","CALL","NOTE","INFO","ALL"];

var DEBUG_CLI_COLORS = [
    undefined,
    clc.xterm(15).bgXterm(4),
    clc.xterm(15).bgXterm(124),
    clc.xterm(252).bgXterm(124),
    clc.xterm(124).bgXterm(0),

    clc.xterm(203).bgXterm(0),
    clc.xterm(11).bgXterm(0),
    clc.xterm(15).bgXterm(0),
    clc.xterm(10).bgXterm(0),
    clc.xterm(14).bgXterm(0),
    clc.xterm(32).bgXterm(0)
];

Engine.colorize = function(string, level){
    return DEBUG_CLI_COLORS && DEBUG_CLI_COLORS[level] ? DEBUG_CLI_COLORS[level](string) : string;
};


Engine.DebugFail = 0;
Engine.DebugTest = 1;
Engine.DebugGoOn = 2;
Engine.DebugConf = 3;
Engine.DebugStub = 4;
Engine.DebugWarn = 5;
Engine.DebugMild = 6;
Engine.DebugCall = 7;
Engine.DebugNote = 8;
Engine.DebugInfo = 9;
Engine.DebugAll = 10;

Engine._cwd = PATH.dirname(require.main ? require.main.filename : __dirname);
process.chdir(Engine._cwd);
Engine._debugEnabled = true;
Engine._debugLevel = 8; // DebugInfo
Engine._debugName = PATH.basename(__filename);

Engine._runParams = {
    "version": undefined,
    "release": undefined,
    "revision": undefined,
    "nodename": undefined,
    "runid": undefined,
    "configname": undefined,
    "usercfgpath": undefined,
    "sharedpath": undefined,
    "configpath": undefined,
    "cfgsuffix": undefined,
    "modulepath": undefined,
    "modsuffix": undefined,
    "logfile": undefined,
    "interactive": undefined,
    "clientmode": undefined,
    "supervised": undefined,
    "runattempt": undefined,
    "lastsignal": undefined,
    "minworkers": undefined,
    "maxworkers": undefined,
    "maxevents": undefined,
    "trackparam": undefined,
    "workpath": undefined
};

Engine.handlers = {};
Engine.queue = [];

log4js.clearAppenders();
Engine.logger = log4js.getLogger('engine');
Engine.logger.setLevel('debug');

Engine.debugger = log4js.getLogger('debugger');

/*
 Static Methods
 */

Engine.debug = function(level,string) {

    var levels = ['fatal','info','fatal','fatal','fatal','error','warn','info','info','debug','trace'];
    Engine.debugger.log(levels[level],string);

    if (Engine.connection) {
        if (Engine.connection.isConnected) {
            if (string === undefined) {
                if (level === undefined) {
                    return;
                } else {
                    string = level; // level was omitted, it should be considered as debug string
                    level = Engine.debugLevel(); // set to current debug level
                }
            }
            var debugString = string.toString();
            if (Engine.debugEnabled()) {
                var time = Engine.toTimestamp(Date.now()) + "." + process.hrtime()[1].toString().substr(0,6);
                if (!(level >= 1 && level <= 10))
                    level = 8;
                if (level <= Engine.debugLevel()) {
                    var prefix = time + " <" + Engine.debugName() + ":" + DEBUG_LEVEL_NAMES[level] + "> ";
                    debugString.split("\n").forEach(function (line, lineNumber) {
                        Engine.connection.output(Engine.colorize(((lineNumber>0 ? "" : prefix) + line),level));
                    });
                }
            }
        }
    }
};


Engine.debugEnabled = function(enabled) {
    if (enabled !== undefined)
        this._debugEnabled = enabled;
    return this._debugEnabled;
};


Engine.debugLevel = function(value) {
    if (value >= 1 && value <= 10)
        this._debugLevel = value;
    return this._debugLevel;
};


Engine.debugName = function(string) {
    if (string !== undefined)
        this._debugName = string;
    return this._debugName;
};


Engine.print_r = function(object) {
    if (Engine.connection && Engine.connection.isReady)
        Engine.connection.output(clc.white(JSON.safeStringify(object,0,1)));
};


Engine.runParams = function(flag) {
    if (flag){
        for (var key in this._runParams) {
            this.connection.setlocal("engine." + key);
        }
    } else {
        return this._runParams;
    }
};


Engine.setInterval = function(callback, interval){
    return setInterval(callback,interval);
};


Engine.configFile = function(module){
    return "./" + module + ".conf";
};


// http://docs.yate.ro/wiki/Javascript_Engine

Engine.atob = function(string){
    var buf = new Buffer(string,'base64');
    return buf.toString('utf8');
};


Engine.btoa = function(string){
    var buf = new Buffer(string);
    return buf.toString('base64');
};


Engine.atoh = function(string){
    return this.btoh(this.atob(string));
};


Engine.htoa = function(string){
    return this.btoa(this.htob(string));
};


Engine.btoh = function(string){
    var buf = new Buffer(string);
    return buf.toString('hex');
};


Engine.htob = function(string){
    var buf = new Buffer(string,'hex');
    return buf.toString('utf8');
};


// Shared vars. Work only inside this script instance Node.js

Engine.shared = {
    _vars_:{}
};


Engine.shared.get = function(name){
    return this._vars_[name];
};


Engine.shared.set = function(name, value){
    this._vars_[name] = value;
};


Engine.shared.clear = function(name){
    delete this._vars_[name];
};


/*
 Extra Methods
 */

Engine.init = function(socketConfig){

    if (Engine.connection) {
        Engine.logger.error("Engine already initiated");
        return;
    }

    if (socketConfig) {
        log4js.addAppender(log4js.appenders.console(),'engine');
    } else {
        log4js.addAppender(function (string) {console.log("%%%%>output:" + log4js.layouts.colouredLayout(string))},'engine');
    }

    Engine.logger.info("Init engine");

    var connection = new YateConnection(socketConfig);
    Engine.connection = connection;

    connection.on("connected", function (message) {
        Engine.logger.info("Connected to Yate on " + (connection.socketConfig?(connection.socketConfig.host+":"+connection.socketConfig.port):"STDIN"));
        if (!connection.socketConfig)
            connection.setlocal("restart", true);
        connection.setlocal("reenter", true);
        connection.setlocal('trackparam',Message.trackName())
        Message.trackName(moduleName);
        Engine.logger.debug("Queued handlers:",JSON.stringify(Engine.handlers));
        for (var messageName in Engine.handlers) {
            Engine.handlers[messageName].forEach(function (handler) {
                if (handler.active) {
                    if (handler.filter) {
                        Engine.connection.install(handler.name, handler.priority, handler.filter, handler.filtervalue);
                        Engine.logger.debug("Installing queued handler:",messageName, handler.priority);
                    }
                    else {
                        Engine.connection.install(handler.name, handler.priority);
                        Engine.logger.debug("Installing queued handler:",messageName, handler.priority, handler.filter, handler.filtervalue);
                    }
                }
            })
        }
        Engine.runParams(true); // collect runParams
    });

    connection.on("ready",function(){
        Engine.logger.info("Engine is ready");
        var message;
        while (message = Engine.queue.shift()){
            Engine.logger.debug("Sending queued message:",message.toString());
            message.dispatch();
        }
    });

    connection.on("connection_error", function (error) {
            Engine.logger.log(this.socketConfig.reconnect?"warn":"error", error.code,this.socketConfig.host + ":" + this.socketConfig.port);
    });

    connection.on("connection_end", function () {
        if (Engine.connection.socketConfig.reconnect){
            Engine.logger.warn("Yate connection closed, will reconnect");
        } else {
            Engine.logger.error("Yate connection closed");
            process.exit(1);
        }
    });

    connection.on("command_error", function (error) {
        Engine.logger.warn(error);
    });

    connection.on("timeout", function (error) {
        Engine.logger.debug("timeout");
    });

    connection.on("reconnect", function (error) {
        Engine.logger.debug("reconnect");
    });

    connection.on("command_unknown", function (error) {
        Engine.logger.error('Unknown command: '+error);
    });

    connection.on("setlocal", function (message) {
        if (!connection.isReady) {
            if (message._name.startsWith("engine.")) {
                Engine._runParams[message._name.substr(7)] = message.value;
                if (Engine.runParams()["workpath"]) {
                    connection.isReady = true;
                    connection.emit("ready");
                }
            }
        }
    });

    connection.on("incoming",function(message){
        var handlers = Engine.handlers[message._name];
        if (handlers) {
            //message.__proto__ = new Message(); //convert basic YateMessage to Engine Message
            var msg = new Message(message);
            handlers.every(function(handler){
                if (typeof handler.callback == "function") {
                    message._handled = handler.callback(msg);
                    if (message._handled)
                        return false; //break from cycle
                }
                return true;
            });
            for (var param in msg){
                if (!(param in Message.prototype)){
                    message.params[param] = msg[param];
                    //Engine.logger.debug(param,msg[param])
                }
            }
            //Engine.logger.debug(msg);
            //Engine.logger.debug(message);
            message._retval = msg._retval;
            message.acknowledge();
        }
    });

    connection.on("shutdown",function(){
        Engine.debug(Engine.DebugTest, "stdin is closed, exiting");
        Engine.logger.warn("stdin is closed, exiting");
        Engine.gracefulShutdown();
    });

    connection.on('raw_message',function(message){
        if (Engine.debugMessages())
            Engine.logger.debug(message);
    });

    Message.install(onCommand, "engine.command", 200);
    Message.install(onDebug, "engine.debug", 200);
    Message.install(onHelp, "engine.help", 200);

    connection.connect();
};


Engine.toBoolean = function (str) {
    if(str==='true'||str==='yes'||str==='on'||str==='enable'||str==='t')
        return true;
    if(str==='false'||str==='no'||str==='off'||str==='disable'||str==='f')
        return false;
    return undefined;
};


Engine.toTimestamp = function (unixtime) {
    var u = new Date(unixtime);
    var y = u.getFullYear();
    var m = u.getMonth(); m++; if (m<10) m='0'+m;
    var d = u.getDate(); if (d<10) d='0'+d;
    var h = u.getHours(); if (h<10) h='0'+h;
    var i = u.getMinutes(); if (i<10) i='0'+i;
    var s = u.getSeconds(); if (s<10) s='0'+s;
    return y+''+m+''+d+''+h+''+i+''+s;
};


Engine.debugMessages = function(enabled) {
    if (enabled !== undefined) {
        this._debugMessages = enabled;
    }
    return this._debugMessages;
};


Engine.gracefulShutdown = function () {
    Engine.logger.warn("graceful shutdown");
    // todo acknowledge all messages or uninstall all handlers
    if (Engine.connection){
        Engine.connection.setlocal("restart", false);
        Engine.connection.setlocal("reenter", false);
        for (var name in Engine.handlers){
            Message.uninstall(name);
        }
        Engine.runParams(); // hack: spend some time to allow existing messages to be acknowledged
        if (Engine.connection.socketConfig){
            Engine.connection.socket.destroy();
        }
    }
    process.exit();
    //todo make good for sockets also
};


/*
 If you want to include native JS scripts from Yate, use Engine.include
 This command tries to solve compatibility issues:
 1. Undefined Object members are not equal to "" or null
 2. #include and other meta-code
 3. Absence of global visibility from included files
 */

if (typeof PATH.isAbsolute != "function"){
    PATH.isAbsolute = function(path) {
        return path.charAt(0) === '/';
    };
}

Engine.include = function(file) {
    Engine.logger.debug("including",file);
    var currentDir = Engine._cwd;
    Engine._cwd = PATH.isAbsolute(PATH.dirname(file)) ? PATH.dirname(file) : PATH.join(Engine._cwd, PATH.dirname(file));
    var filename = PATH.basename(file);
    Engine.logger.debug("Change current dir",Engine._cwd);
    process.chdir(Engine._cwd);
    try {
        var code = fs.readFileSync(filename).toString();
    } catch (err) {
        Engine.logger.error("Can't open file " + err.path);
        return;
    }
    code = code.replace(/^#require '(.*)'$/gm,"Engine.include('$1')");
    code = code.replace(/^#require "(.*)"$/gm,"Engine.include('$1')");
    code = code.replace(/^#(.*)$/gm,"//$1");
    code = code.replace(/\((.*)==.*""\)/gm,"(!$1)");
    code = code.replace(/\((.*)===.*null\)/gm,"(!$1)");
    code = code.replace(/\((.*)!=.*""\)/gm,"($1)");
    vm.runInThisContext(code, file);
    Engine._cwd = currentDir;
    Engine.logger.debug("Restore current dir",Engine._cwd);
    process.chdir(currentDir);
}.bind(this);


/*

 http://docs.yate.ro/wiki/Javascript_Message

 */

//Message.prototype = new YateMessage();

function Message(message) {
    this._msgtime = Date.now();
    if (typeof message == "string") {
        //YateMessage.call(this,name); // Engine message inherits YateMessage, including constructor
        //this._connection = Engine.connection;
        this._name = message;
    } else {
        this._name = message._name;
        this._retval = message._retval;
        for (var param in message.params) {
            if (!(param in Message.prototype)) {
                this[param] = message.params[param];
            }
        }
    }
}

Message.prototype = {
    _name: undefined,
    _msgtime: undefined,
    _retval: undefined
};

Message._trackName = "nodejs";


/*
 Static Methods
*/

Message.trackName = function(string){
    if (string !== undefined) {
        Message._trackName = string;
        if (Engine.connection && Engine.connection.isReady)
            Engine.connection.setlocal('trackparam',string)
    }
    return Message._trackName;
};


Engine.debugName = function(string) {
    if (string !== undefined)
        Message._debugName = string;
    return Message._debugName;
};


Message.install = function(callback, name, priority, filter, filtervalue) {
    if (typeof callback != "function")
        return;
    var active = true;
    if (Engine.handlers[name]){
        Engine.handlers[name].some(function(handler){
            if (priority < handler.priority) { // less means more prioritized
                handler.active = false;
                if (Engine.connection){
                    if (Engine.connection.isConnected) {
                        Engine.connection.uninstall(name); // uninstall because later will reinstall with higher priority
                    }
                }
            } else {
                active = false;
            }
        })
    } else {
        Engine.handlers[name] = [];
        active = true;
    }

    Engine.handlers[name].push({callback: callback, name: name, priority: priority, active: active, filter: filter, filtervalue: filtervalue});
    Engine.handlers[name].sort(function(a,b){
        if (a.priority < b.priority) {
            return -1;
        }
        if (a.priority > b.priority) {
            return 1;
        }
        return 0;
    });

    if (active && Engine.connection && Engine.connection.isConnected) {
        Engine.connection.install(name, priority, filter, filtervalue);
    }
};


Message.uninstall = function(name) {
    delete Engine.handlers[name];
    if (Engine.connection) {
        if (Engine.connection.isReady) {
            Engine.connection.uninstall(name);
        }
    }
};


/*
 Methods
*/

Message.prototype.enqueue = function(callback) {
    if (Engine.connection.isReady) {
        var msg = new YateMessage(this.name());
        for (var param in this){
            if (!(param in Message.prototype)){
                msg.params[param] = this[param];
            }
        }
        msg._connection = Engine.connection;
        msg.dispatch();
        //msg = undefined; // todo ?
    } else {
        Engine.queue.push(this);
    }
    if (typeof callback == "function") {
        var that = this;
        Engine.connection.on('answer', function(message) {
            if (message._id == that._id) {
                if (message._handled)
                    callback(message);
            }
        })
    }
};


Message.prototype.name = function(string) {
    if (typeof string == "string") {
        this._name = string.toString();
    }
    return this._name;
};


Message.prototype.broadcast = function() {
    Engine.logger.warn("broadcast() method not implemented");
};


Message.prototype.retValue = function(string) {
    if (string) {
        this._retval = string.toString();
    }
    return this._retval;
};


Message.prototype.msgTime = function() {
    return this._msgtime;
};


Message.prototype.getColumn = function(row) {
    Engine.logger.warn("getColumn() method not implemented");
};


Message.prototype.getRow = function(row) {
    if (this.userData)
        return this.userData[row];
};


Message.prototype.getResult = function(row,col) {
    if (this.userDataArray)
        return this.userDataArray[row][col];
};


// override Object toString() method

Message.prototype.toString = function() {
    return this.name() + " " + JSON.stringify(this.params());
};


/*

 http://docs.yate.ro/wiki/Javascript_File

 */

function File() {
}

File.exists = function(path){
    try { fs.accessSync(path,fs.R_OK) } catch (e) {return false}
    return true;
};

File.remove = function(path){
    try { fs.unlinkSync(path) } catch (e) {return false}
    return true;
};

File.rename = function(oldName,newName){
    try { fs.renameSync(oldPath, newPath) } catch (e) {return false}
    return true;
};

File.mkdir = function(path,mode){
    // todo: mode not tested
    try { fs.mkdirSync(path,mode) } catch (e) {return false}
    return true;
};

File.rmdir = function(path){
    try { fs.rmdirSync(path) } catch (e) {return false}
    return true;
};

File.getFileTime = function(path){
    var result = -1;
    try { result = Math.floor(fs.statSync(path).ctime.getTime() / 1000) } catch (e) {}
    return result;
};


File.setFileTime = function(path,time){
    var millitime = time * 1000;
    //Set file last change time
    //time: File time in seconds since EPOCH
    try { fs.utimesSync(path,millitime,millitime) } catch (e) {return false}
};


/*

http://docs.yate.ro/wiki/Javascript_Hasher

*/

function Hasher(algorithm){
    if (['md5','sha1','sha256'].indexOf(algorithm) == -1)
        algorithm = 'md5';
    this.hasher = crypto.createHash(algorithm);
    this.algorithm = algorithm;
}

Hasher.prototype.update = function(data){
    if (data) {
        this.hasher.update(data.toString());
    }
};

Hasher.prototype.hexDigest = function(){
    return this.hasher.digest('hex');
};


Hasher.prototype.clear = function(){
    this.hasher = crypto.createHash(this.algorithm);
};


/*

http://docs.yate.ro/wiki/Javascript_JSON

*/

JSON.loadFile = function(name){
    return JSON.parse(fs.readFileSync(name, 'utf8'));
};


JSON.safeStringify = function(obj,f,spaces){
    var cache = [];
    return JSON.stringify(obj,function(key, value) {
        if (typeof value === 'object' && value !== null) {
            if (cache.indexOf(value) !== -1) {
                // Circular reference found, discard key
                return;
            }
            cache.push(value);
        }
        return value;
    },spaces);
};


JSON.saveFile = function(name,obj,spaces){
    var filename = PATH.isAbsolute(name) ? name : PATH.join(Engine._cwd,name);
    return fs.writeFileSync(filename,JSON.safeStringify(obj,undefined,spaces),'utf8');
};


/*

 http://docs.yate.ro/wiki/Javascript_XML

 */


var domParser = new DOMParser();
var xmlSerializer = new XMLSerializer();

function XML(obj_tag_xml){
    var that = this;
    if (typeof obj_tag_xml == 'string') {
        that.dom = domParser.parseFromString(obj_tag_xml);
    } else {
        that.dom = obj_tag_xml;
    }
}

XML.prototype.getAttribute = function(name){
    var attr = this.dom.firstChild.getAttributeNode(name);
    return attr ? attr.value : null;
};

XML.prototype.attributes = function(){
    return this.dom.firstChild.attributes;
};

XML.prototype.getChild = function(){
    return new XML(this.dom.firstChild);
}

XML.prototype.getText = function(){
    return this.dom.firstChild.textContent;
};

XML.prototype.getChildren = function(){
    var nodes = this.dom.childNodes;
    var result = [];
    for (var i=0; i < nodes.length; i++) {
        result.push(new XML(nodes.item(i)))
    }
    return result;
};

XML.prototype.xmlText = function(){
    return xmlSerializer.serializeToString(this.dom);
};

XML.prototype.toString = function(){
    return this.xmlText();
};



/*

String extension

*/

String.prototype.startsWith = function(string) {
    return this.indexOf(string) === 0;
};


/*

Console command support

*/

var helpText =
    "  nodejs [stop]\r\n" +
    "  debug nodejs [on|off|level 1-10]\r\n" +
    "Controls Node.js external engine operation";


// longer command should go first to work
var commandsHelp = {
    "nodejs stop": " Gracefully stops Node.js script",
    "nodejs": helpText
};


var commandsAutoComplete = {
    "debug": moduleName,
    "help nodejs": "stop",
    "help": moduleName,
    "nodejs": "stop"
};


var commandsExecute = {
    "nodejs stop": function(string,message){
        message._handled = true;
        message.retValue("Exiting Node.js script...\r\n");
        message.acknowledge();
        Engine.gracefulShutdown()
    },
    "nodejs": function(string){return helpText}
};


function onCommand(message){
    if (message.line) {
        for (var line in commandsExecute) {
            if (message.line.startsWith(line)) {
                message.retValue(commandsExecute[line](message.line.substr(line.length).trim(),message)+ "\r\n");
                return true;
            }
        }
    } else {
        // auto-complete
        onComplete(message,message.partline,message.partword);
    }
}


function onHelp(message){
    if (message.line) {
        for (var line in commandsHelp) {
            if (message.line.startsWith(line)) {
                message.retValue(commandsHelp[line] + "\r\n");
                return true;
            }
        }
    }
}


function onDebug(message){
    if (message.module && message.module != moduleName)
        return;
    if (!message.line)
        return;
    var level = Engine.debugLevel();
    var enable = Engine.debugEnabled();
    var newEnable = Engine.toBoolean(message.line);
    if (newEnable !== undefined) {
        enable = newEnable;
        Engine.debugEnabled(enable);
    }
    else if (message.line.startsWith("level")) {
        level = message.line.substr(5).trim();
        Engine.debugLevel(level);
    }
    else {
        return false;
    }
    message.retValue("Module " + moduleName + " debug " + enable + " level " + level + "\r\n");
    //return true;
}


function onComplete(msg,partline,partword) {
    partline = partline || "help";
    if (commandsAutoComplete[partline]) {
        commandsAutoComplete[partline].split(" ").some(function(word){
            oneCompletion(msg, word, partword);
        });
    }
}


function oneCompletion(msg,word,partword) {
    if (partword && word.indexOf(partword) != 0 || (word == partword))
        return; // do not add word if partword is not empty and NOT part of it's beginning (index 0)
    if (word == partword)
        return;
    var ret = msg.retValue();
    if (ret != "")
        ret += "\t";
    msg.retValue(ret + word);
}


/*

Warning!!! Math.random() override

*/

Math.random_original = Math.random;

Math.random = function(min, max){
    return Math.floor(min + Math.random_original()*(max-min))
};


/*

Provide ctrl-c handler for Windows environment

*/

if (process.platform === "win32") {
    var rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });
    rl.on("SIGINT", function () {
        process.emit("SIGINT");
    });
}


process.on("SIGINT", function () {
    Engine.logger.warn("Caught SIGINT");
    Engine.gracefulShutdown();
});


module.exports.Yate = Yate;
module.exports.YateConnection = YateConnection;
module.exports.YateMessage = YateMessage;

module.exports.Message = Message;
module.exports.Engine = Engine;
module.exports.Hasher = Hasher;
module.exports.File = File;
module.exports.XML = XML;
module.exports.log4js = log4js;

