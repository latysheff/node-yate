# node-yate
This is Node.js connector to [Yate](http://yate.ro/opensource.php) telephony engine

## Why

- Develop and debug native Yate [Javascript](http://docs.yate.ro/wiki/Javascript) programs in Node.js
- Extend Yate functionality with rich Node.js capabilities, provided by [modules](https://www.npmjs.com/).

## Features
- Colorized output with cli-color npm resembles Yate debug
- Based on above, Engine.debug() looks like native. Console debug management is supported:
```
debug nodejs [on|off|level 1-10]
```
- Some extra console commands
```
nodejs stop
```
- Internal debugger, based on log4js npm, writes to cli and optionally to file
```
Engine.logFile("mylog.txt");
Engine.logger.warn('Warning!');
```
- Auto-reconnect, if used in TCP socket mode
## Installation
Ensure that nodejs and npm are installed. For Centos:
```
yum install nodejs npm
```
Then install node-yate module with examples:
```
npm install https://bitbucket.org/latysheff/node-yate/get/master.tar.gz -g
```
or
```
cd /usr/src
git clone https://bitbucket.org/latysheff/node-yate.git
npm install node-yate -g
```

## Example

```
#!/usr/bin/node
var engine = require("node-yate");
Engine = engine.Engine;
Message = engine.Message;
Engine.init();
Engine.connection.on("ready",function() {
    Engine.include("./yate/eliza.js")
});
```
Look also into examples directory

## Execute
Go to Yate console:
```
telnet 0 5038
```
Launch script from Yate console:
```
external start /usr/lib/node_modules/node-yate/examples/example.js
```

## Test it
Type in Yate console:
```
hello
```
You should get something like:
```
Hi, there, I'm Eliza.
```
## Network operation
Edit extmodule.conf:
```
[listener external]
type=tcp
addr=0.0.0.0
port=10500
role=global
```
According to your environment provide socket config to Engine.init():
```
Engine.init({
    host: "10.192.168.1",
    port: 10500,
    reconnect: true
});
```

## Plain extmodule operation
If you just want extmodule without native Yate javascript support, her is an example:
```
#!/usr/bin/node

var extmodule = require("node-yate/extmodule");
var YateConnection = extmodule.YateConnection;

var connection = new YateConnection();
connection.connect();

var logfunc = console.log;
console.log = function(){
    logfunc.apply(console, ["%%%%>output:"].concat([].slice.call(arguments)));
};

connection.on("connected",function() {
    connection.setlocal("reenter", true);
    connection.install("engine.command",100);
    connection.install("my.message",100);
    var message = connection.createMessage("my.message");
    message.myparam = "myvalue";
    message.dispatch()
});

connection.on("incoming",function(message) {

    if (message._name == "engine.command" && message.line == "test")
    {
        message._handled = true;
        message._retval = "passed\r\n";
    }
    else if (message._name == "my.message")
    {
        console.log(message.params());
        message._handled = true;
        message.extraparam = "extravalue";
    }
    message.acknowledge()
});

connection.on("raw_message",function(message) {
    //console.log(message)
});
```
You can find it in examples directory. Run it with
```
external start /usr/lib/node_modules/node-yate/examples/extmodule.js
```
## API
todo
## Limitations
- 'debug threshold' doesn't have affect on module's pseudo-debugging activity. Use 'debug level' instead.
- Message.dispatch() is not implemented due to Node.js pure async nature
- If you install multiple message handlers, they will work, but highest priority handler will also suppress, or postpone handlers, installed from other modules, until all handlers in Node.js scripts will be executed, even if they have lower priority.
