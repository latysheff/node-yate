#!/usr/bin/node
var engine = require("../../node-yate");
Engine = engine.Engine;
Message = engine.Message;

Engine.debugMessages(false);
Engine.init();

Engine.connection.on("ready",function(){
/*
     eliza.js is a chatbot, included in Yate distribution as an example of native Javascript
     But with node-yate library you can run it on Node.js as well
     Please note, that you have to include it with special Engine method.
     See details in node-yate/index.js about Engine.include.
*/
    Engine.include("./yate/eliza.js")
});
