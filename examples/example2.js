#!/usr/bin/node

/*

This example is a kind of test of various Yate Javascript objects and methods

 */

var engine = require("../../node-yate");

var Engine = engine.Engine;
var Message = engine.Message;
var Hasher = engine.Hasher;
var File = engine.File;
var XML = engine.XML;
var log4js = engine.log4js;

log4js.loadAppender('file');
//log4js.addAppender(log4js.appenders.file("./my.log"));

if (process.env.NODE_ENV == 'development') {
    var config = {
        "host": "10.192.169.112",
        "port": 1052,
        "reconnect": true
    };
}

Engine.init(config);
Engine.debugMessages(false);

Engine.connection.on("ready",function(){

    // Yate debugging is active only when connected. For this example we ensure it by catching 'ready' event

    for (var i=1; i<=10; i++) {
        Engine.debug(i, "This is debug message at level " + i);
    }

    var h = new Hasher('sha256');
    h.update(Date.now(),false);
    var hex1 = h.hexDigest();
    Engine.debug(Engine.DebugTest, "hexDigest('sha256') of Date.now() is " + hex1);

    Engine.shared.set("test", "passed");
    Engine.debug(Engine.DebugTest, 'Engine.shared.get("test") is ' + Engine.shared.get("test"));
    Engine.debug(Engine.DebugTest, "multiline debug: line1\r\nline2");
    Engine.debug(Engine.DebugTest, 'Engine.btoh("Hello, World!") is ' + Engine.btoh("Hello, World!"));
    Engine.debug(Engine.DebugTest, File.getFileTime('/tmp/'))

    var x = new XML('<main b="c">test</main><child>zz</child><child/>');
    var ch = x.getChild();
    Engine.debug(Engine.DebugTest,ch + '');
    Engine.debug(Engine.DebugTest,ch.xmlText());
    Engine.debug(Engine.DebugTest,x.getAttribute('b'));
    Engine.debug(Engine.DebugTest,x.getText());
    var children = x.getChildren();
    for (var idx in children) {
        var child = children[idx];
        Engine.debug(Engine.DebugTest,'[' + child.xmlText() + ']');
    }



});

Message.install(function(m){
    m.id = "777";
    return true;
}, "ping");

var m = new Message("ping");
m.id = "666";
m.enqueue(function(m){
    Engine.logger.info("pong", m.id);
});


Message.trackName("tests");
Message.install(onCommand,"engine.command",100);

Message.install(onMyMessage,"my.message",100);

var m = new Message("my.message");
m.text = "Hello! I am custom message!";
m.enqueue();

JSON.saveFile('message.json',m,1);
File.remove('message.json');

Engine.debugName("Node.js test");

var testfile = '/tmp/test';
File.mkdir(testfile);
File.setFileTime(testfile,(Date.now()/1000) - 3600);
var d = File.getFileTime(testfile) * 1000;
Engine.logger.info('getFileTime ' + d);

var dt = new Date(d);
Engine.logger.info('Date = '+dt);



function onMyMessage(message) {
    Engine.debug(Engine.DebugTest,message.text + " (" + message.name() + ") msgTime()=" + message.msgTime());
    message.myextraparam = "This is additional paramater";
    return true;
}

function onCommand(message) {
    if (message.line == "test") {
        Engine.debug(Engine.DebugTest,"command from console: " + message.line);
        message.retValue("test passed\r\n");
        message.mymessage = "my module saw this message";
        return true;
    }
}
