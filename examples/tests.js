#!/usr/bin/node

var engine = require("node-yate");
var config;
Engine = engine.Engine;
Message = engine.Message;
Hasher = engine.Hasher;

Engine.debugMessages(false);
Engine.init(config);
Engine.logFile("/tmp/mylog.txt");
Engine.logger.setLevel("debug");

Engine.connection.on("ready",function(){

    // Yate debugging is active only when connected

    for (var i=1; i<=10; i++) {
        Engine.debug(i, "This is debug message at level " + i);
    }

    var h = new Hasher('sha256');
    h.update(Date.now(),false);
    var hex1 = h.hexDigest();
    Engine.debug(Engine.DebugTest, "hexDigest('sha256') of Date.now() is " + hex1);

    Engine.shared.set("test", "passed");
    Engine.debug(Engine.DebugTest, 'Engine.shared.get("test") is ' + Engine.shared.get("test"));
    Engine.debug(Engine.DebugTest, "multiline debug: line1\r\nline2");
    Engine.debug(Engine.DebugTest, 'Engine.btoh("Hello, World!") is ' + Engine.btoh("Hello, World!"));

});

Message.install(function(m){
    m.id = "777";
    return true;
}, "ping");

var m = new Message("ping");
m.id = "666";
m.enqueue(function(m){
    Engine.logger.info("pong", m.id);
});


Message.trackName("tests");
Message.install(onCommand,"engine.command",100);

Message.install(onMyMessage,"my.message",100);

var m = new Message("my.message");
m.text = "Hello! I am custom message!"
m.enqueue();

JSON.saveFile('/tmp/message.json',m,1);

Engine.debugName("Node.js tests");

function onMyMessage(message) {
    Engine.debug(Engine.DebugTest,message.text + " (" + message.name() + ") msgTime()=" + message.msgTime());
    message.myextraparam = "This is additional paramater";
    return true;
}

function onCommand(message) {
    if (message.line == "test") {
        Engine.debug(Engine.DebugTest,"command from console: " + message.line);
        message.retValue("test passed\r\n");
        message.mymessage = "my module saw this message";
        return true;
    }
}
