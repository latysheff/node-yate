#!/usr/bin/node

var extmodule = require("../../node-yate");
var YateConnection = extmodule.YateConnection;

var connection = new YateConnection();

var logfunc = console.log;
console.log = function(){
    logfunc.apply(console, ["%%%%>output:"].concat([].slice.call(arguments)));
};

connection.on("connected",function() {
    connection.setlocal("reenter", true);
    connection.install("engine.command",100);
    connection.install("my.message",100);
    var message = connection.createMessage("my.message");
    message.myparam = "myvalue";
    message.dispatch()
});

connection.on("incoming",function(message) {
    if (message._name == "engine.command" && message.line == "test")
    {
        message._handled = true;
        message._retval = "passed\r\n";
    }
    else if (message._name == "my.message")
    {
        console.log(message.params());
        message._handled = true;
        message.extraparam = "extravalue";
    }
    message.acknowledge()
});

connection.on("raw_message",function(message) {
    //console.log(message)
});

connection.connect();
